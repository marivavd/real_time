import 'package:flutter/material.dart';
import 'package:real_time/home/data/repository/supabase.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});



  @override
  State<MyHomePage> createState() => _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePage> {
  List<Map<String, dynamic>> sp = [];

  @override
  void initState() {
    super.initState();
    subsribeorder((modelOrder) =>
    {
      setState(() {
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          getOrders().then((value) =>
          {
            setState(() {
              sp = value;
            })
          });
        });
      })
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverList.builder(
            itemCount: sp.length,
              itemBuilder: (_, index){
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(sp[index]['id']),
                  Text(sp[index]['address']),
                  Text(sp[index]['country']),
                  Text(sp[index]['phone']),
                  Divider(color: Colors.black,)
                ],
              );
          })
        ],
      ),
    );
  }
}
