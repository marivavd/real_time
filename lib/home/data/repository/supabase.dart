
import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;
void subsribeorder(callback){
  supabase
      .channel("changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.update,
      schema: "public",
      table: "orders",
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}

Future<List<Map<String, dynamic>>> getOrders() async{
  return await supabase.
  from("orders").
  select();
}